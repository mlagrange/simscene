from setuptools import setup

setup(
    name='simscene',
    version='0.1',
    scripts=['simscene.py'],
    author='Emmanouil Chourdakis',
    license='GPLv3'
)
    

simscene-py

*WARNING* This is work-in-progress. Has a lot of bugs and is nowhere as mature as the matlab version.

An acoustic pattern generation tool
Copyright (c) Emmanouil Theofanis Chourdakis <e.t.chourdakis@qmul.ac.uk>

simscene-py is a collection of tools to synthesize an audio scene from independent sound events.
It is inspired by Lagrange et al. simScene (https://bitbucket.org/mlagrange/simscene) but takes
a different direction. The goal of simscene-py is to (1) have a python implementation of simScene
since python is the platform of choice for developers accustomed with deep learning packages
or other python package candy and (2) to allow generating audio scenes from patterns.

Please direct any comments, suggestions, etc. to Emmanouil Chourdakis <e.t.chourdakis@qmul.ac.uk>

This code is licensed under GPLv3. Please see LICENSE for more info.

Usage examples:

      *	./simscene.py -h
      Displays command line syntax and option descriptions.

      * ./simscene.py example/sound/ output/ 10 -e example/forest_events.xls -b example/forest_backgrounds.xls -N 10 -v
      Creates wavefiles and plots of 10 instances of scenes for which events are described in example/forest_events.xls and
      background sounds in example/forest_backgrounds.xls


 

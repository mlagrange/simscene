SimScene is a set of Matlab functions dedicated to the simulation of acoustic scenes. 

This tool is specifically tailored to the evaluation of machine listening systems. Thus, extensive and precise annotation of the scene content is provided together with the simulated scene.

This tool is a research tool, provided as-is with the hope that it will be useful and easy to adapt to the needs of others. Do not hesitate to provide us with feedback at the following mail address: mathieu.lagrange@cnrs.fr

SimScene have been tested on Matlab R2013b. The input audio files must be sampled at 44100 Hz.
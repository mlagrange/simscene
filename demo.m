% SimScene is a set of Matlab functions dedicated to the simulation of acoustic scenes.
% This tool is specifically tailored to the evaluation of machine listening systems.
% Thus, extensive and precise annotation of the scene content is provided together with the simulated scene.
%
% This tool is a research tool, provided as-is with the hope that it will be
% useful and easy to adapt to the needs of others. Do not hesitate to provide
% us with feedback at the following mail address: mathieu.lagrange@cnrs.fr
%
% SimScene have been tested on Matlab R2013b. The input audio files must be sampled at 44100 Hz.

% clear all;
% close all;

inputPath='./sound/';
outputPath='./results/';

%% timeMode: 'generate' -- ebrMode: 'generate'

timeMode='generate';
ebrMode='generate';
sceneDuration=15;

score.sceneDuration=sceneDuration;
score.backgrounds={{'forest','forest', 1}};
score.events={{'alouette','alouette', 12, 0, 2, 0.1, 1, 10, 0, 0},...
              {'bulbul'  ,'bulbul'  , 6 , 0, 2, 0, 3, 13, 0, 0},...
              {'geai'    ,'geai'    , 6 , 0, 1, 0.25, 5, 10, 0, 0}};

sceneId = ['demo_forest_timeMode_' timeMode '_ebrMode_' ebrMode];
disp(sceneId);

simScene(inputPath,outputPath,score,...
    'timeMode',timeMode,'ebrMode',ebrMode,'outputFileName',sceneId);

%% timeMode: 'replicate' -- ebrMode: 'replicate'

% instanceAnnotFile='./scene2Replicate/forest.txt';
% instanceAudioFile='./scene2Replicate/forest.wav';
% 
% timeMode='replicate';
% ebrMode='replicate';
% sceneId = ['demo_forest_timeMode_' timeMode '_ebrMode_' ebrMode];
% disp(sceneId);
% 
% score.backgrounds={{'forest','forest', 1}};
% 
% score.events={{'alouette','', -3, 0, 0, 0, 0, 0, 0, 0},... % Only change <ebr>
%               {'bulbul','',  3, 0, 0, 0, 6, 0, 0, 0}}; % Change <ebr> and <start_time>
% 
% % If score.event is of length 1 and <label>='', offsets will be used for all classes.
% % score.events={{'','',-6,0,0,0,0,0,0,0}}; % Change <ebr> for the three sound classes of bird.
% 
% simScene(inputPath,outputPath,score,...
%     'timeMode',timeMode,'ebrMode',ebrMode,'outputFileName',sceneId,...
%     'instanceAnnotFile',instanceAnnotFile,'instanceAudioFile',instanceAudioFile);
% 

%% timeMode: 'abstract' -- ebrMode: 'abstract'
% 
% instanceAnnotFile='./scene2Replicate/forest.txt';
% instanceAudioFile='./scene2Replicate/forest.wav';
% 
% timeMode='abstract';
% ebrMode='abstract';
% sceneId = ['demo_forest_timeMode_' timeMode '_ebrMode_' ebrMode];
% disp(sceneId);
% 
% score.backgrounds={{'forest','forest', 1}};
% score.events={{'geai','', -3, 0, 5, 0, 1, 0, 0, 0}}; % Change <ebr>, <mean_time_between_instances> and <start_time>
% 
% simScene(inputPath,outputPath,score,...
%     'timeMode',timeMode,'ebrMode',ebrMode,'outputFileName',sceneId,...
%     'instanceAnnotFile',instanceAnnotFile,'instanceAudioFile',instanceAudioFile);
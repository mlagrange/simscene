function [o] = getSampleInfo(label,query,sampleType,inputData,trackLength,eventInfo,sr)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

o.isBackground = strcmp(sampleType,'background');
o.classLabel = label;
o.startTimes = [];
o.endTimes = [];
o.names={};
o.query= query;
o.trackLength=trackLength;
path = [inputData sampleType '/'];

if any(strcmp(query,{eventInfo.query}))
    o.startTimes=eventInfo(strcmp(query,{eventInfo.query})).startTimes;
    o.endTimes=eventInfo(strcmp(query,{eventInfo.query})).endTimes;
    o.names=eventInfo(strcmp(query,{eventInfo.query})).names;
else
    
    if ~exist(path, 'dir')
        error(['Could not find local dir ',path,'\n']);
    end
    
    samples = dir([path '*wav']);
    samplesInd = find(cellfun('isempty',regexpi({samples.name},query,'match'))==0);
    
    for ii=1:length(samplesInd)
        samplesInfo=audioinfo([path,'/',samples(samplesInd(ii)).name]);
        
        if sr~=samplesInfo.SampleRate
            error('Sample Rate must be 44100')
        end
        
        o.startTimes(end+1) = 0;
        o.endTimes(end+1) = samplesInfo.TotalSamples/samplesInfo.SampleRate;
        o.names{end+1}=samples(samplesInd(ii)).name;
    end
end

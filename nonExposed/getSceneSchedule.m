function [sceneSchedule] = getSceneSchedule(sceneSchedule,sceneObjects,settings,time,indReplicate)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

if strcmp(settings.ebrMode,'replicate')
    sceneSchedule(settings.schedPos).ebr=settings.ebrs(indReplicate);
else
    sceneSchedule(settings.schedPos).ebr= settings.ebr_mean+randn*settings.ebr_std;
end

switch settings.randomFlag
    case {'random'} % Random choice of sample
        inst = randi(length(sceneObjects(settings.classId).startTimes));
    case {'random-unique'} % Random choice of sample without repetition
        instances=[sceneSchedule(1:end-1).instance];
        instanceDone=instances([sceneSchedule(1:end-1).classId]==length(sceneObjects));
        inst=1:length(sceneObjects(end).endTimes);
        inst(arrayfun(@(x) any(instanceDone==x),inst))=[];
        if isempty(inst)
            error(['using samplechoice : random-unique; Not enough isolated samples for the class' sceneObjects(end).classLabel])
        else
            inst=inst(randperm(length(inst),1));
        end
    case {'close'} % Choose the sample closest in duration to the annotation.
        [~,inst] = min(abs(sceneObjects(settings.classId).endTimes-sceneObjects(settings.classId).startTimes-(settings.end_times(indReplicate)-settings.start_times(indReplicate))));
end

switch settings.timeMode
    case 'generate'
        dur = sceneObjects(settings.classId).endTimes(inst)-sceneObjects(settings.classId).startTimes(inst);
    case 'abstract'
        dur = sceneObjects(settings.classId).endTimes(inst)-sceneObjects(settings.classId).startTimes(inst);
        if(dur-settings.template_duration_mean-settings.template_duration_std>5 )
            dur= settings.template_duration_mean+randn*settings.template_duration_std;
            disp('Sample Duration - meanDuration - stdDuration > 5: cut sample')
        end
    case 'replicate'
        dur = settings.end_times(indReplicate)-settings.start_times(indReplicate);
end


sceneSchedule(settings.schedPos).classId=settings.classId;
sceneSchedule(settings.schedPos).classLabel=settings.label;
sceneSchedule(settings.schedPos).isBackground = 0;
sceneSchedule(settings.schedPos).instance = inst;
sceneSchedule(settings.schedPos).position = time;
sceneSchedule(settings.schedPos).duration=dur;

%% Fade I/O

if (time < settings.start_time+settings.fade_in_time && settings.fade_in_time>0)
    sceneSchedule(settings.schedPos).ebr = -20 + ((time-settings.start_time)/settings.fade_in_time) * (sceneSchedule(settings.schedPos).ebr+20);
end
if (time > settings.end_time-settings.fade_out_time && settings.fade_out_time>0)
    sceneSchedule(settings.schedPos).ebr = -20 + ((settings.end_time-time)/settings.fade_out_time) * (sceneSchedule(settings.schedPos).ebr+20);
end

%% check ebr
if (~isfinite(sceneSchedule(settings.schedPos).ebr))
    error('Error while computing ebr; ebr = +/-inf')
end

end


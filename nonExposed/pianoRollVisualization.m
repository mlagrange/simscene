function [] = pianoRollVisualization(sceneObjects,sceneSchedule,score,figNum,settingFigure,figuresOption,fileName)


%% Producing a "piano roll" visualization

switch figuresOption
    case 1
        f=figure('Visible', 'off');
    case 2
        f=figure(figNum);
end
clf;
grid on;

for i=1:length(sceneObjects)
    text(0, i+.4, [num2str(sceneObjects(i).classLabel), '  '], 'HorizontalAlignment', 'right');
end

for i=1:length(sceneSchedule)
    id = sceneSchedule(i).classId;
    if (sceneSchedule(i).isBackground)
        rectangle('Position', [0, id+.2, score.sceneDuration, .6], 'FaceColor', settingFigure.cmap(id,:));
    else
        t1 = sceneSchedule(i).position;
        rectangle('Position', [t1, id+.1, sceneSchedule(i).duration, .8], 'FaceColor', settingFigure.cmap(id,:));
    end
end

xlim([0 score.sceneDuration])
set(gca,'YTick', [],'YTicklabel', [],'xtick',0:20:score.sceneDuration);
xlabel('time (sec)')

if figuresOption == 1
    set(f,'PaperUnits','centimeters')
    set(f,'PaperPositionMode','manual')
    set(f,'papersize',[settingFigure.width,settingFigure.height])
    set(f,'paperposition',[0,0,settingFigure.width,settingFigure.height])
    set(findall(f,'-property','FontSize'),'FontSize',settingFigure.FontSize)
    set(findall(f,'-property','FontName'),'FontName','Arial')
    
    print(f,fileName,'-dpng')
end



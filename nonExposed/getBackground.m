function [sceneSchedule,sceneObjects,score]=getBackground(inputPath,score,eventInfo,sr,noiseLevel,noiseFiltMaxFreq,noiseFiltOrder)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

if isfield(score,'backgrounds')
    
    for i=1:length(score.backgrounds)
        sceneObjects(i) = getSampleInfo(score.backgrounds{i}{1},score.backgrounds{i}{2},'background',inputPath,score.sceneDuration,eventInfo,sr);
        sceneSchedule(i).classId=i;
        sceneSchedule(i).classLabel=score.backgrounds{i}{2};
        sceneSchedule(i).position=0;
        sceneSchedule(i).instance=1;
        sceneSchedule(i).isBackground = 1;
        sceneSchedule(i).duration = score.sceneDuration;
        if (i==1)
            sceneSchedule(i).ebr=1;
        else
            sceneSchedule(i).ebr= score.backgrounds{i}{3};
        end
    end
    
else
    
    noise=randn(1,sr*score.sceneDuration);
    noise=noise./max(abs(noise));
    
    if noiseFiltMaxFreq
        noise=filter(fir1(noiseFiltOrder,noiseFiltMaxFreq/sr/2),1,noise);
    end
    
    noise=noise.*power(noiseLevel/20);
    
    sceneObjects(1).isBackground = 1;
    sceneObjects(1).classLabel = 'noise';
    sceneObjects(1).startTimes = 0;
    sceneObjects(1).endTimes = score.sceneDuration;
    sceneObjects(1).names={'noise'};
    sceneObjects(1).query= 'noise';
    sceneObjects(1).trackLength=score.sceneDuration;
    
    sceneSchedule(1).classId=1;
    sceneSchedule(1).classLabel='noise';
    sceneSchedule(1).position=0;
    sceneSchedule(1).instance=1;
    sceneSchedule(1).isBackground = 1;
    sceneSchedule(1).duration = score.sceneDuration;
    
    score.backgrounds{1}{1}='noise';
    score.backgrounds{1}{2}=noise;
end
end
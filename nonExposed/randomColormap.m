function cmap = randomColormap (sceneObjects)
% returns a colormap with randomly chosen colors somewhat evenly spread in
% the color spectrum, to be used for the graphical representations of a
% synthesized scene.

% Background sounds will have a saturation of .3, foreground sounds of 1
% Value is always let to .5, since modifying it would erroneously suggest
% a difference of intensity

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

nbTracks = length(sceneObjects);

% Colors for figures
% Choosing color hues evenly spread in the color spectrum, then randomizing their order
hues = (0:1/nbTracks:1);
hues = hues(1:nbTracks);
hues = hues(randperm(nbTracks));
% Generate a colormap:
for c=1:nbTracks
    if (sceneObjects(c).isBackground)
        cmap(c,:) = hsl2rgb([hues(c), .3, .5]);
    else
        cmap(c,:) = hsl2rgb([hues(c), 1, .5]);
    end
end
end
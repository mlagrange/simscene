function [ eventsLocation ] = getEventsLocation(signalLength,sr,onset,offset)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

%% events location: LOGICAL
% 1 : Events
% 0 : BG

%% Init events Location
eventsLocation=zeros(1,signalLength);

for ii=1:length(onset)
    eventsLocation(round(onset(ii)*sr+1):round(offset(ii)*sr))=1;
end

%% Logical conversion
eventsLocation=logical(eventsLocation);
end


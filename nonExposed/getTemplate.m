function [template] = getTemplate(instanceAnnotFile,instanceAudioFile)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

%% get audioFile
[signal,sr] = audioread(instanceAudioFile);
if(size(signal,2)==2)
    signal=mean(signal,2);
end
signal=0.99*signal/(max(abs(signal)));
template.sceneDuration=length(signal)/sr;

%% get onset/offset times and background Location
[onset,offset,classNames]=loadEventsList(instanceAnnotFile);
bgLocation=(~getEventsLocation(length(signal),sr,onset,offset));

%% Get Template
[uniqueClassNames,~,ib]=unique(classNames);
template.class=cell(1,length(uniqueClassNames));

for ii=1:length(uniqueClassNames)
    template.class{ii}=cell(1,13);
    
    onsetTmp=onset(ib==ii);
    offsetTmp=offset(ib==ii);
    
    if(length(onsetTmp)>1)
        meanSpacingTime = mean(onsetTmp(2:end)-onsetTmp(1:end-1));
        stdSpacingTime = std(onsetTmp(2:end)-onsetTmp(1:end-1));
    else
        meanSpacingTime = -1;
        stdSpacingTime = 0;
    end
    
    [ebr]=getEbrs(bgLocation,signal,sr,onsetTmp,offsetTmp);
    
    template.class{ii}={uniqueClassNames{ii},uniqueClassNames{ii},mean(ebr),std(ebr),meanSpacingTime,stdSpacingTime,...
        onsetTmp(1),offsetTmp(end),0,0,onsetTmp,offsetTmp,ebr};
end


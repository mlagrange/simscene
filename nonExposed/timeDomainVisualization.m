function status = timeDomainVisualization(tracks,figNum,settingFigure,figuresOption,fileName)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

% Producing a time domain visualization
step=0.5; % seconds
wStep = round(step*settingFigure.sr);
wSize = wStep*2;
for t=1:size(tracks,1)
    for i=0:floor((size(tracks,2)-wSize)/wStep)
        % Not really power, but more nicely additive, better suited for
        % this representation I think
        powers(i+1,t) = norm(tracks(t,i*wStep+1:min(size(tracks,2), i*wStep+wSize)));
    end
end

powers(powers<max(powers(:))/500) = 0;
powers = gaussianSmoothing(powers', 15)';

switch figuresOption
    case 1
       f=figure('Visible', 'off');
    case 2
       f=figure(figNum);
end


clf;
h = area(powers,'LineWidth', 2, 'EdgeColor', [.2, .2, .2]);
for t=1:size(tracks,1)
    h(t).FaceColor = settingFigure.cmap(t, :);
end

xlim([0 size(powers,1)])
xtick=0:round(20/step):size(powers,1); % every 20 sec
set(gca,'YTick', [],'YTicklabel', [],'xtick',xtick,'xticklabel',xtick*step);
xlabel('time (sec)')
% ylabel('amplitude')

set(findall(f,'-property','FontSize'),'FontSize',settingFigure.FontSize)
set(findall(f,'-property','FontName'),'FontName','Arial')


box off %// remove outer border
set(gca,'ycolor', [1 1 1]);
ax1 = gca;
yruler = ax1.YRuler;
yruler.Axle.Visible = 'off';

if figuresOption == 1
    set(f,'PaperUnits','centimeters')
    set(f,'PaperPositionMode','manual')
    set(f,'papersize',[settingFigure.width,settingFigure.height])
    set(f,'paperposition',[0,0,settingFigure.width,settingFigure.height])
    set(gca,'ycolor', [1 1 1]);
    print(f,fileName,'-dpng')
end


function as = gaussianSmoothing(as, factor)

if ~exist('factor', 'var'), factor = 200; end

% gaussian filtering for smooth display
g = gausswin(ceil(size(as, 2)/factor)); % shall adapt the size to the length of the file
g = g/sum(g);
for k=1:size(as, 1)
    as(k, :) = conv(as(k, :), g, 'same');
end



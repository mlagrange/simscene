function [] = checkcOverlapping(sceneSchedule,sceneDuration)
%% check overlapping
onset=[sceneSchedule(2:end).position];
offset=[sceneSchedule(2:end).duration]+onset;
[~,index]=sort(onset);
onset=onset(index);
offset=offset(index);
coveringIndice=onset(2:end)-offset(1:end-1);
if(find(coveringIndice<0))
    error('covering is not respected')
end

%% check sceneDuration
if(offset(end)>sceneDuration+0.1)
    error('Last event length is superior to the scene Duration')
end


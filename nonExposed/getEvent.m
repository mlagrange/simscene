function [sceneSchedule,sceneObjects]=getEvent(sceneSchedule,sceneObjects,inputPath,score,timeMode,ebrMode,randomFlag,eventInfo,sr)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

settings.randomFlag = randomFlag;
settings.schedPos = length(sceneSchedule)+1;

for ii=1:length(score.events)
    settings.classId =ii+length(score.backgrounds);
    settings.ebrMode=ebrMode;
    settings.timeMode=timeMode;
    settings.label=score.events{ii}{1};
    
    settings.ebr_mean = score.events{ii}{3};
    settings.ebr_std = score.events{ii}{4};
    
    sceneObjects(settings.classId) = getSampleInfo(score.events{ii}{1}, score.events{ii}{2},'event',inputPath,score.events{ii}{8},eventInfo,sr);
    settings.duration_mean = mean(sceneObjects(settings.classId).endTimes-sceneObjects(settings.classId).startTimes);
    settings.duration_std =  std(sceneObjects(settings.classId).endTimes-sceneObjects(settings.classId).startTimes);
    settings.mean_time_between_instances = score.events{ii}{5};
    settings.time_between_instances_std = score.events{ii}{6};
    settings.start_time = score.events{ii}{7};
    settings.end_time = score.events{ii}{8};
    settings.fade_in_time = score.events{ii}{9};
    settings.fade_out_time = score.events{ii}{10};
    
    if ~strcmp(settings.timeMode,'generate') || ~strcmp(settings.ebrMode,'generate')
        settings.start_times = score.events{ii}{11};
        settings.end_times  = score.events{ii}{12};
        settings.ebrs = score.events{ii}{13};
        settings.template_duration_mean=mean(settings.end_times-settings.start_times);
        settings.template_duration_std=std(settings.end_times-settings.start_times);
    end
    
    if (settings.start_time > score.sceneDuration)
        settings.start_time = score.sceneDuration;
        fprintf(2, ['Sart time of event class ' score.events{ii}{1} ' is superior to the scene Duration; start time has been set to the scene duration.\n'])
    end
    
    if (settings.end_time > score.sceneDuration)
        settings.end_time = score.sceneDuration;
        fprintf(2, ['End time of event class ' score.events{ii}{1} ' is superior to the scene Duration; end time has been set to the scene duration.\n'])
    end
    
    
    
    %% generate sceneSchedule
    switch timeMode
        
        case {'generate','abstract'}
            
            if (settings.mean_time_between_instances == 0) % Looping: only for generate timeMode
                if strcmp(timeMode,'abstract')
                    error('simScene cannot loop event in abstract time Mode')
                end
                time = settings.start_time;
                while time<settings.end_time
                    if time+sceneSchedule(settings.schedPos).duration > settings.end_time
                        break;
                    end
                    [sceneSchedule] = getSceneSchedule(sceneSchedule,sceneObjects,settings,time);
                    time = time+sceneSchedule(settings.schedPos).duration;
                    settings.schedPos = settings.schedPos+1;
                end
                
            elseif (settings.mean_time_between_instances < 0) % Just once: abstract and generate
                [sceneSchedule] = getSceneSchedule(sceneSchedule,sceneObjects,settings,settings.start_time);
                settings.schedPos = settings.schedPos+1;
                
            else % General case: abstract and generate
                t = settings.start_time;
                time = t;
                while t<settings.end_time
                    if (time<0)
                        time=0;
                    end
                    if time > settings.end_time
                        break;
                    end
                    [sceneSchedule] = getSceneSchedule(sceneSchedule,sceneObjects,settings,time);
                    t = t+settings.mean_time_between_instances;
                    time=t+settings.time_between_instances_std*randn;
                    settings.schedPos = settings.schedPos+1;
                end
            end
            
        case 'replicate'
            
            for jj=1:length(settings.start_times)              
                if settings.start_times(jj) <= settings.end_time && settings.start_times(jj) >= settings.start_time
                    [sceneSchedule] = getSceneSchedule(sceneSchedule,sceneObjects,settings,settings.start_times(jj),jj);
                    settings.schedPos = settings.schedPos+1;
                end
            end
    end
end
end
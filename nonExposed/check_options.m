function [] = check_options(timeMode,ebrMode,score)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.


%% Forbidden modes :
% time abstract - ebr replicate
% time generate - ebr replicate

%% Check mode validity
if(~strcmp(ebrMode,'abstract') && ~strcmp(ebrMode,'replicate') && ~strcmp(ebrMode,'generate'))
    error('wrong ebrMode')
end

if(~strcmp(timeMode,'abstract') && ~strcmp(timeMode,'replicate') && ~strcmp(timeMode,'generate'))
    error('wrong ebrMode')
end

if(strcmp(timeMode,'abstract') && strcmp(ebrMode,'replicate'))
    error(['wrong timeMode, timeMode must be set to replicate if ebrMode is set to replicate (The number of events must be the same); time=' timeMode ', and ebr=' ebrMode])
elseif(strcmp(timeMode,'generate') && strcmp(ebrMode,'replicate'))
    error(['wrong timeMode, timeMode must be set to replicate if ebrMode is set to replicate (The number of events must be the same); time=' timeMode ', and ebr=' ebrMode])
end

%% Check options validity
if ~isfield(score,'sceneDuration')
    if strcmp(timeMode,'generate') && strcmp(timeMode,'generate')
        error('Score does not have a sceneDuration field')
    else
        fprintf(2, 'score.sceneduration does not exist; sceneDuration will be set to the duration of the scene to replicate.\n')
    end
end

if strcmp(timeMode,'generate') || strcmp(timeMode,'generate')
    if  ~isfield(score,'events')
        error('score.event does not exist; score.event is needed if one of the modes is set to generate')
    end
end

if ~strcmp(timeMode,'generate') || ~strcmp(timeMode,'generate')
    if isfield(score,'events')
        fprintf(2, 'score.event exist; score.events cells values will be used as offsets.\n')
    end
end




function [] = coloredSpectrumVisualization(tracks,figNum,settingFigure,figuresOption,fileName,sr)

wStep=1024;
wSize=wStep*2;
maxFreqBin=400;

% Producing a colored spectrum visualization

for i=1:size(tracks,1)
    spec = log(1+abs(spectrogram(tracks(i,:), hanning(wSize), wStep, wSize)));
    spec = min(1, spec ./ max(spec(:)));
    spec = spec(1:maxFreqBin,:);
    spec = flipud(spec);
    % black background
%     for colorComp=1:3
%         if (i==1)
%             img(:,:,colorComp) = settingFigure.cmap(i,colorComp)*spec;
%         else
%             img(:,:,colorComp) = img(:,:,colorComp)+settingFigure.cmap(i,colorComp)*spec;
%         end
%     end
    %white background
    if i==1
        img = ones(size(spec, 1), size(spec, 2), 3)*.4;
    end  
    for colorComp=1:3 
        img(:,:,colorComp) = (1-spec).*img(:,:,colorComp)+settingFigure.cmap(i,colorComp)*spec;
    end
end




img = img/max(img(:));

switch figuresOption
    case 1
       f=figure('Visible', 'off');
    case 2
       f=figure(figNum);
end


clf;
imagesc(img);
xlim([0 size(img,2)])
ylim([0 maxFreqBin])
xtick=0:round(settingFigure.sr/wStep*20):size(img,2); % every 20 sec
ytick=0:50:maxFreqBin; % every 500 Hz
set(gca,'YTick',0:50:maxFreqBin,'YTicklabel', fliplr(round(ytick*settingFigure.sr/wSize)),'xtick',xtick,'xTicklabel',round(xtick*wStep/settingFigure.sr));
xlabel('time (sec)')
ylabel('Frequency (Hz)')

set(findall(f,'-property','FontSize'),'FontSize',settingFigure.FontSize)
set(findall(f,'-property','FontName'),'FontName','Arial')

if figuresOption == 1
    set(f,'PaperUnits','centimeters')
    set(f,'PaperPositionMode','manual')
    set(f,'papersize',[settingFigure.width,settingFigure.height])
    set(f,'paperposition',[0,0,settingFigure.width,settingFigure.height])
    
    print(f,fileName,'-dpng')
end


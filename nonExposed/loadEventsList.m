function [onset,offset,classNames] = loadEventsList(filename)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

% Open raw file
fid = fopen(filename,'r+');

% Read 1st line
tline = fgetl(fid);
onset_offset(:,1) = sscanf(tline, '%f\t%f\t%*s');
classNames{1} = char(sscanf(tline, '%*f\t%*f\t%s')');

% Read rest of the lines
i=1;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    if (ischar(tline))
        onset_offset(:,i) = sscanf(tline, '%f\t%f\t%*s');
        classNames{i} = char(sscanf(tline, '%*f\t%*f\t%s')');
    end;
end

% Split onset_offset
onset = onset_offset(1,:)';
offset = onset_offset(2,:)';

% Close file
fclose(fid);
clear all
close all

addpath(genpath('~/projets/simscene'))

inputPath='~/Dropbox/databases/environment/dcase2/dcase2_train_dev/train/sound/event/';

label={'alert','clearthroat','cough','keyboard','doorslam','drawer','keys','knock','laughter','pageturn','phone','speech'};


if ~exist(inputPath, 'dir')
    error(['Could not find local dir ',path,'\n']);
end


for jj=1:length(label)
    eventInfo(jj).query = label{jj};
    
    samples = dir([inputPath '*wav']);
    samplesInd = find(cellfun('isempty',regexpi({samples.name}, eventInfo(jj).query,'match'))==0);
    
    eventInfo(jj).startTimes=[];
    eventInfo(jj).endTimes=[];
    eventInfo(jj).names={};
    
    for ii=1:length(samplesInd)
        samplesInfo=audioinfo([inputPath samples(samplesInd(ii)).name]);
        
        eventInfo(jj).names{end+1} = samples(samplesInd(ii)).name;
        eventInfo(jj).startTimes(end+1) = 0;
        eventInfo(jj).endTimes(end+1) = samplesInfo.TotalSamples/samplesInfo.SampleRate;
    end
end

save('data/eventInfo','eventInfo')
disp('')

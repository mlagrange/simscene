function [EBRs] = getEbrs(bgLocation,signal,sr,onset,offset)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

EBRs=zeros(length(onset),1);

%% get EBR of all the events of a class
for ii=1:length(onset)
    eventLocation=zeros(1,length(signal));
    eventLocation(round(onset(ii)*sr+1):round(offset(ii)*sr))=1;
    eventLocation=logical(eventLocation);
    EBRs(ii) = ebr(signal(eventLocation),signal(bgLocation),1,1); 
end

end


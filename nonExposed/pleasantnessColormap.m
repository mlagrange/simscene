function cmap = pleasantnessColormap (sceneObjects)
% returns a colormap following pleasantness indicators
% red: mechanical sounds
% blue: human sounds
% yellow: animal sounds

% Background sounds will have a saturation of .3, foreground sounds of 1
% Value is always let to .5, since modifying it would erroneously suggest
% a difference of intensity

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

nbTracks = length(sceneObjects);

classLabels = {'train' 'crowd' 'schoolyard', 'traffic', 'park', 'stepPark', 'scooter', 'moto', 'bike', 'subway', 'bus', 'streetNoise', 'hammer', 'barrier', 'glassNoise', 'waterNoise', 'cutleryNoise', 'keys', 'music', 'brake', 'pneumaticBlast', 'bip', 'whistle', 'zip', 'voice', 'baby', 'cityCar', 'truck', 'carHorn', 'constructionSite', 'coughing', 'stopCar', 'doorCar', 'siren', 'doorHouse', 'bird', 'dog', 'cityStep', 'bell', 'broom', 'suitcase', 'plane'};
classType = [0 1 1 0 2 1 0 0 1 0 0 0 0 0 0 0 1 1 0 0 0 0 0 1 1 0 0 0 0 1 0 0 0 0 1 2 2 1 1 0 1 0]+1;

% for k=1:length(classLabels)
%    fprintf('%s ', [classLabels{k} ' ' num2str(classType(k))])
% end

hues = [0 240 66]/360;
% Generate a colormap:
for c=1:nbTracks
%          sceneObjects(c).classLabel
    idx = find(contains(classLabels, sceneObjects(c).classLabel));
%          classType(idx)
    if isempty(idx)
%         disp(sceneObjects(c).classLabel)
cmap(c,:) = zeros(1, 3);
    else
    %     if (sceneObjects(c).isBackground)
    %         cmap(c,:) = hsl2rgb([hues(classType(idx)), .7, .5]);
    %     else
    cmap(c,:) = hsl2rgb([hues(classType(idx)), 1, .5]);
    end
    %     end
end
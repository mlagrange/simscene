function s = maxWindowEBR(fg, bg, offset, fgScale)
% Computes the maximum EBR on 1/4s windows of a FG relative to a BG when
% that FG is at a given position in time.
% Optional argument fgScale allows to compute what the EBR would be if FG
% was scaled by that much.

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin==3) fgScale=1; end
len=10000;
for k=1:floor(2*length(fg)/len)
    t1 = (len/2)*(k-1)+1;
    t2 = min(t1+len-1, length(fg));
    if (t2-t1>10) snrs(k) = snr(fg(t1:t2), bg, offset+t1, fgScale); end
end
s = max(snrs);
end

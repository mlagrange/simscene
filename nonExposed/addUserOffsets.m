function [scoreEvent] = addUserOffsets(template,scoreEvent,timeMode,ebrMode)

% This program was written by Mathias Rossignol & Grégoire Lafay
% is Copyright (C) 2015 IRCAM <http://www.ircam.fr>
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
% for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program.  If not, see <http://www.gnu.org/licenses/>.

%% Create userOffsets matrix
userOffset=zeros(length(template),4);

if length(scoreEvent)==1 && strcmp('',scoreEvent{1}{1})  % add the same offset to all the classes
    userOffset=[repmat(scoreEvent{1}{3},length(template),1) repmat(scoreEvent{1}{4},length(template),1) repmat(scoreEvent{1}{5},length(template),1) repmat(scoreEvent{1}{6},length(template),1)];
    fprintf(2, 'score.event if of length 1: classId will set to the class labels of the scene to replicate.\n')
else % add class wise offsets
    for jj=1:length(scoreEvent) scoreEventClass{jj}=scoreEvent{jj}{1}; end;
    for ii=1:length(template)
        indClass=find(strcmp(template{ii}{1},scoreEventClass));
        if ~isempty(indClass)
            userOffset(ii,1:4)=[scoreEvent{indClass}{3} scoreEvent{indClass}{4} scoreEvent{indClass}{5} scoreEvent{indClass}{6}];
            if ~strcmp('',scoreEvent{indClass}{2})
                template{ii}{2}=scoreEvent{indClass}{2};
            end
            if scoreEvent{indClass}{7} ~= 0 % change class start time
                template{ii}{7}= scoreEvent{indClass}{7};
            end
            if scoreEvent{indClass}{8} ~= 0 % change class end time
                template{ii}{8}= scoreEvent{indClass}{8};
            end
            if scoreEvent{indClass}{9} ~= 0 % change fade in time
                template{ii}{9}= scoreEvent{indClass}{9};
            end
            if scoreEvent{indClass}{10} ~= 0 % change fade out time
                template{ii}{10}= scoreEvent{indClass}{10};
            end
        else
            userOffset(ii,1:4)=zeros(1,4);
        end
    end
end

%% add user offsets to time
switch timeMode
    case 'abstract'
        for ii=1:length(template)
            template{ii}{5}=template{ii}{5}+userOffset(ii,3);
            template{ii}{6}=template{ii}{6}+userOffset(ii,4);
        end
end

%% add user offsets to ebr
switch ebrMode
    case 'generate'
        for ii=1:length(template)
            template{ii}{3}=userOffset(ii,1);
            template{ii}{4}=userOffset(ii,2);
        end
    case 'abstract'
        for ii=1:length(template)
            template{ii}{3}=template{ii}{3}+userOffset(ii,1);
            template{ii}{4}=template{ii}{4}+userOffset(ii,2);
        end
    case 'replicate'
        for ii=1:length(template)
            template{ii}{13}=template{ii}{13}+userOffset(ii,1)+randn(length(template{ii}{13}),1)*userOffset(ii,2);
            template{ii}{3}=mean(template{ii}{13});
            template{ii}{4}=std(template{ii}{13});
        end
end

scoreEvent=template;


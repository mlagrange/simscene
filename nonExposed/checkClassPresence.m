function [] = checkClassPresence( sceneSchedule,sceneObjects )

classIndex=ones(1,length(sceneObjects));
classIndex(unique([sceneSchedule.classId]))=0;

if(find(classIndex==1))
    classLabel= {sceneObjects.classLabel};
    disp({classLabel{logical(classIndex)}})
    fprintf(2, 'At least one class has been removed during the simulation process\n')
end
    

